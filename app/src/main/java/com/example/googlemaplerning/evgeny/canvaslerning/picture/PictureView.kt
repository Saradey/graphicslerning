package com.example.googlemaplerning.evgeny.canvaslerning.picture

import android.content.Context
import android.graphics.*
import android.graphics.Paint.ANTI_ALIAS_FLAG
import android.view.View


class PictureView(context: Context) : View(context) {


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        //Мы можем записать операции рисования на канве в некий шаблон,
        // а затем многократно воспроизводить его.

        //В манифесте для Activity необходимо прописать такую строку
        //android:hardwareAccelerated="false"
        //Без нее не будет работать.

        //беспечивающий сглаживание диагональных линий, рисуемых объектом Paint
        // (снижая при этом производительность).
        val p = Paint(ANTI_ALIAS_FLAG)
        p.style = Paint.Style.STROKE
        p.strokeWidth = 5f

        val picture = Picture()

        // beginRecording начинаем запись. Этот метод возвращает нам канву, на ней мы
        // и будем выполнять все операции, которые будут записаны. А на вход методу
        // beginRecording необходимо передать ширину и высоту изображения, которое
        // вы собираетесь записывать. Т.е. наш шаблон будет размером 300х300.
        val canvasSecond = picture.beginRecording(300, 300)
        val path = Path()
        path.moveTo(170f,100f)
        path.lineTo(300f, 300f)
        path.lineTo(200f, 300f)
        path.lineTo(50f, 100f)
        path.lineTo(170f, 100f)
        canvasSecond.drawPath(path, p)
        picture.endRecording()

        canvas?.drawPicture(picture)
        canvas?.translate(300f, 300f)
        //задаем размеры этих областей, и в эти размеры будет смасштабировано изображение
        canvas?.drawPicture(picture, RectF(300f, 300f, 400f, 400f))

        //воспроизведение Picture может дать прибавку к скорости, по сравнению
        // с вызовом методов канвы напрямую.
    }


}