package com.example.googlemaplerning.evgeny.canvaslerning.bitmap

import android.content.Context
import android.graphics.*
import android.util.Log
import android.view.View
import com.example.googlemaplerning.evgeny.canvaslerning.R

class BitmapView(context: Context) : View(context) {


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        //Bitmap – это объект, который хранит в себе изображение.
        // Та же канва, с которой мы обычно работаем, это обертка,
        // которая принимает команды от нас и рисует их на Bitmap

        //Для получения изображения из файла используется фабрика BitmapFactory.
        // У нее есть несколько decode* методов, которые принимают на вход массив
        // байтов, путь к файлу, поток, файловый дескриптор или идентификатор ресурса.

        //decodeFile(String pathName) – получить Bitmap из файла, указав его путь.
        // Т.е. этим методом можем считать картинку с SD-карты.
        // (getExternalStorageDirectory)

        //decodeResource(Resources res, int id) – получить Bitmap из drawable-ресурса,
        // указав его ID. Этот метод вернет нам картинку из папок res/drawable
        // нашего приложения.

        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.woman)

        //getWidth – ширина картинки в px
        //getHeight – высота картинки в px
        //getByteCount – число байт, которое занимает картинка (доступен только с API Level 12)
        //getRowBytes – число байт в одной строке картинки
        //getConfig – инфа о способе хранения данных о пикселах
        Log.d(
            "BitmapView",
            "width: ${bitmap.width} height: ${bitmap.height} byteCount: ${bitmap.byteCount} rowBytes: ${bitmap.rowBytes} config: ${bitmap.config}"
        )

        val matrix = Matrix()
        matrix.postScale(2f, 3f)


        val paint = Paint(Paint.ANTI_ALIAS_FLAG)

        canvas?.drawARGB(80, 102, 204, 255)
        canvas?.drawBitmap(bitmap, 50f, 50f, paint)
        canvas?.translate(200f, 200f)
        canvas?.drawBitmap(bitmap, matrix, paint)
        canvas?.translate(200f, 200f)

        //rectSrc со сторонами равными половине сторон картинки. Т.е. этот прямоугольник
        // охватывает левую верхнюю четверть картинки. Эту часть мы будем брать для
        // вывода на экран далее в примере. А выводить мы ее будем в прямоугольник
        // rectDst, это просто произвольная область на экране.
        canvas?.drawBitmap(
            bitmap, Rect(
                0, 0,
                bitmap.getWidth() / 2,
                bitmap.getHeight() / 2
            ), Rect(300, 100, 500, 200), paint
        )
    }


}