package com.example.googlemaplerning.evgeny.canvaslerning

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.googlemaplerning.evgeny.canvaslerning.bitmap.ReadBitMap

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        viewMy = DrawViewRectPaint(this)
//        setContentView(viewMy)
//        setContentView(ClassPath(this))
//        setContentView(MatrixView(this))
//        setContentView(CanvaView(this))
//        setContentView(RegionView(this))
//        setContentView(ClipView(this))
//        setContentView(TextViewMain(this))
//        setContentView(ColorFilterColorMatrix(this))
//        setContentView(SaveBitmap(this))
        setContentView(ReadBitMap(this))


    }

}
