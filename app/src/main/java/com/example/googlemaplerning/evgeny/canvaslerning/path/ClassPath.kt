package com.example.googlemaplerning.evgeny.canvaslerning.path

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.view.View

class ClassPath(private val con: Context) : View(con) {

    private var path: Path = Path()
    private var paint: Paint = Paint()


    override fun onDraw(canvas: Canvas?) {
        canvas?.drawColor(Color.GREEN)
        paint.style = Paint.Style.FILL
        paint.color = Color.BLACK
        paint.strokeWidth = 10f
        paint.textSize = 25f
        canvas?.drawText("0x 0y", 0f, 0f, paint)
        canvas?.drawText("100x 0y", 100f, 10f, paint)
        canvas?.drawText("200x 0y", 200f, 10f, paint)
        canvas?.drawText("300x 0y", 300f, 10f, paint)
        canvas?.drawText("400x 0y", 400f, 10f, paint)
        canvas?.drawText("500x 0y", 500f, 10f, paint)
        canvas?.drawText("600x 0y", 600f, 10f, paint)
        canvas?.drawText("700x 0y", 700f, 10f, paint)
        canvas?.drawText("800x 0y", 800f, 10f, paint)
        canvas?.drawText("900x 0y", 900f, 10f, paint)
        canvas?.drawText("0x 0y", 1000f, 10f, paint)
        canvas?.drawText("0x 100y", 0f, 100f, paint)
        canvas?.drawText("0x 200y", 0f, 200f, paint)
        canvas?.drawText("0x 300y", 0f, 300f, paint)
        canvas?.drawText("0x 400y", 0f, 400f, paint)
        canvas?.drawText("0x 500y", 0f, 500f, paint)
        canvas?.drawText("0x 600y", 0f, 600f, paint)
        canvas?.drawText("0x 700y", 0f, 700f, paint)
        canvas?.drawText("0x 800y", 0f, 800f, paint)
        canvas?.drawText("0x 900y", 0f, 900f, paint)
        canvas?.drawText("0x 1000y", 0f, 1000f, paint)

//        path.moveTo(100f, 100f)
//        path.lineTo(100f, 100f)
//        path.lineTo(100f, 100f)

        //триугольник
//        path.moveTo(100f, 100f)
//        path.lineTo(150f, 200f)
//        path.lineTo(50f, 200f)

        canvas?.let {
            //            it.drawPath(path, paint)
//            path.reset()
//
//            //где стартовая точка
//            path.moveTo(250f, 100f)
//            //где точка линии
//            path.lineTo(300f, 200f)
//            path.lineTo(200f, 200f)
//
////            закрываем подфигуру методом close.
////            path.close()
//            it.drawPath(path, paint)


//            path.reset()
//            path.moveTo(500f, 500f)
//            path.lineTo(600f, 500f)
//            path.lineTo(600f, 600f)
//            path.lineTo(500f, 600f)
//            it.drawPath(path, paint)
//            path.reset()

            //кривые безье
//            path.moveTo(100f, 100f)
//            // квадратичная кривая
//
//            path.quadTo(300f, 600f, 600f, 100f)
//            paint.style = Paint.Style.STROKE
//            paint.color = Color.BLACK
//            canvas.drawPath(path, paint)

//            paint.color = Color.RED
//            canvas.drawPoint(300f, 600f, paint)
//            canvas.drawPoint(600f, 100f, paint)

//
//            // кубическая кривая
//            path.reset()
//            path.moveTo(400f, 400f)
//            path.cubicTo(550f, 650f, 900f, 200f, 1100f, 400f)
//            paint.color = Color.BLACK
//            canvas.drawPath(path, paint)
//
//            paint.color = Color.RED

            val text = "Hello World Hellow World Hellow Hellow"

            paint.color = Color.BLUE
            paint.textSize = 25f
            paint.style = Paint.Style.FILL
            path.addCircle(500f, 500f, 100f, Path.Direction.CCW)
            paint.style = Paint.Style.STROKE
            canvas.drawTextOnPath(text, path, 0f, 100f, paint)

            path.reset()

        }


    }

}
