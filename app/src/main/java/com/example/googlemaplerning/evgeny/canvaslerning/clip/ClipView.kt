package com.example.googlemaplerning.evgeny.canvaslerning.clip

import android.content.Context
import android.graphics.*
import android.view.View

class ClipView(private val mainContext: Context) : View(mainContext) {

    val p = Paint()
    val rect = Rect(200, 300, 600, 600)

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.drawRGB(31, 135, 209)

        p.color = Color.BLUE
        p.strokeWidth = 5f

        drawGrid(canvas)


        p.color = Color.RED
        p.style = Paint.Style.STROKE
        canvas?.drawRect(rect, p)



        p.color = Color.BLUE
        canvas?.translate(300f, 500f)

        // задание clip-области
        //говорим канве, что теперь рисование доступно только в этой области
        canvas?.clipRect(rect)
        //deprecated
//        canvas?.clipRect(Rect(200, 300, 600, 600), Region.Op.UNION)

        //Чтобы убрать clip-область, можно использовать метод restore.

        drawGrid(canvas)
    }


    private fun drawGrid(canvas: Canvas?) {
        val range = 50..800
        for (index in range step 50) {
            canvas?.drawLine(
                (100 + index).toFloat(),
                100f,
                (100 + index).toFloat(),
                600f,
                p
            )
        }

        val range2 = 50..500
        for (index in range2 step 50) {
            canvas?.drawLine(
                100f,
                (100 + index).toFloat(),
                900f,
                (100 + index).toFloat(),
                p
            )
        }
    }


}
