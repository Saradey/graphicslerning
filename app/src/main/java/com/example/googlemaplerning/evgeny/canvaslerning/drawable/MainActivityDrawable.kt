package com.example.googlemaplerning.evgeny.canvaslerning.drawable

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.googlemaplerning.evgeny.canvaslerning.R

class MainActivityDrawable : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity_drawable)
        //Drawable – это абстрактный контейнер для графического объекта.
        // Его главное абстрактное умение – он может нарисовать свое
        // содержимое на предоставленной ему канве.
        //А вот что конкретно он нарисует, зависит уже от реализации.
        // Это может быть, например, тот же Bitmap, если мы используем
        // BitmapDrawable объект.
        // Или это может быть простая геометрическая фигура,
        // если мы используем ShapeDrawable.

        //Самое распространенное использование Drawable – это свойство background

        //- если это цвет, то создаст ColorDrawable,
        //- если это id картинки в res/drawable, то создаст BitmapDrawable
        //- если это id xml-файла в res/drawable, то распарсит его и вернет
        // соответствующего ему наследника Drawable: StateListDrawable,
        // AnimationDrawable или какой-то другой
//        imvDrawableTest.setImageResource(R.drawable.my_shape_rectangle)
//        imvDrawableTest.setImageResource(R.drawable.my_shape_oval)
//        imvDrawableTest.setImageResource(R.drawable.my_shape_line)
//        imvDrawableTestSecond.setImageResource(R.drawable.main_ring)

        //GradientDrawable можем указать больше трех цветов для градиента.
//        val gradient = GradientDrawable(
//            GradientDrawable.Orientation.BL_TR,
//            intArrayOf(Color.RED, Color.GREEN, Color.BLUE, Color.CYAN, Color.MAGENTA)
//        )
//        gradient.shape = GradientDrawable.RECTANGLE
//        gradient.gradientType = GradientDrawable.LINEAR_GRADIENT
//        gradient.cornerRadius = 40f
//        gradient.setStroke(10, Color.BLACK, 20f, 5f)
//        imvDrawableTestSecond.setImageDrawable(gradient)


        //Мы можем в коде получать доступ к отдельным Drawable внутри LayerDrawable.
        // Для этого сначала получаем LayerDrawable.
        //LayerDrawable layerDrawable = (LayerDrawable) view.getBackground();
        //А затем вызываем метод findDrawableByLayerId(int id) и указываем id,
        // который вы указывали в атрибуте id тега item. На выходе получим Drawable.
        //Также у LayerDrawable есть еще несколько интересных методов
        //getNumberOfLayers() - возвращает кол-во Drawable-слоев
        //setDrawableByLayerId(int id, Drawable drawable) -
        // заменяет Drawable по id слоя (протестить)

//У View, кстати, есть методы, которые позволяют программно управлять состоянием.
// Это, например: setPressed и setSelected.


    }


}