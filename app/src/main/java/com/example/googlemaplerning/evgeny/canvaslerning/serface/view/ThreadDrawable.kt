package com.example.googlemaplerning.evgeny.canvaslerning.serface.view

import android.graphics.Canvas
import android.graphics.Color
import android.util.Log
import android.view.SurfaceHolder

class ThreadDrawable(
    private val holderSurface: SurfaceHolder,
    var running: Boolean = false,
    var canvas: Canvas? = null
) : Thread() {


    override fun run() {

        while (!running.not()) {
            Log.d("ThreadDrawable", "draw")
            canvas = null
            canvas = holderSurface.lockCanvas()

            if (canvas == null)
                continue
            canvas?.drawColor(Color.BLUE)

            //отобразит наши художества
            holderSurface.unlockCanvasAndPost(canvas)
        }
    }


}
