package com.example.googlemaplerning.evgeny.canvaslerning.create.bitmap

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Paint
import android.util.DisplayMetrics
import android.view.View
import com.example.googlemaplerning.evgeny.canvaslerning.R

class HowCreateBitmap(context: Context) : View(context) {

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)


        //1) создание на основе другого Bitmap
        //2) создание из массива цветов
        //3) создание пустого Bitmap

        //Создание на основе другого Bitmap
        //Эти методы позволяют нам взять готовый Bitmap и скопировать его целиком
        // или частично в новый bitmap. При этом поддерживаются преобразования с
        // помощью матрицы.

        //source – Bitmap-источник, от которого будем брать его часть
        //x,y – координаты точки, от которой будем отсчитывать часть
        //width, height – высота и ширина части
        //m – матрица для применения преобразований к взятой части
        //filter – если true, то будет включено сглаживание при scale- и rotate-
        // преобразованиях матрицы (но это приведет к некоторой потере в производительности)
        //createBitmap(Bitmap source, int x, int y, int width, int height, Matrix m, boolean filter)

        //Аналогичен методу 1, но без использования матрицы и фильтра.
        // createBitmap(Bitmap source, int x, int y, int width, int height)

        //В итоге мы получим тот же Bitmap, что и источник.
        //createBitmap(Bitmap src)

        //В итоге мы получим тот же Bitmap, что и источник, но он будет нужных нам
        // размеров, заданных параметрами dstWidth и dstHeight.
        //createScaledBitmap(Bitmap src, int dstWidth, int dstHeight, boolean filter)

        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.woman)
        val newBitmap = Bitmap.createScaledBitmap(bitmap, 2000, 2000, true)
        canvas?.drawBitmap(bitmap, 50f, 50f, paint)
        canvas?.translate(200f, 200f)
        canvas?.drawBitmap(newBitmap, 100f, 100f, paint)


        //Создание из массива цветов
        //display – объект DisplayMetrics, из которого Bitmap возьмет значение densityDpi
        //colors – массив цветов, т.е. фактически массив пикселов из которых будет состоять созданный Bitmap
        //offset – отступ от начала colors при чтении его значений
        //stride – шаг, который будет использован для перемещения по массиву при смене строки Bitmap
        //width, height – размеры создаваемого Bitmap
        //config – конфигурация
        //Рассмотрим пример, где параметр offset будем считать равным 0, ширину bitmap = 100, stride = 150.
        //Система создает Bitmap и заполняет его цветами из массива построчно. Но элементы массива она берет не все подряд,
        // а высчитывает индекс первого элемента для каждой новой строки по формуле:
        //индекс первого элемента для каждой строки = (номер строки – 1) * stride

        //createBitmap(DisplayMetrics display, int[] colors, int offset, int stride, int width, int height, Bitmap.Config config)

        //тоже самое но без stride и без offset
        //createBitmap(DisplayMetrics display, int[] colors, int width, int height, Bitmap.Config config)

        //Аналогичен методу 1, но без использования display. без stride и без offset
        //createBitmap(int[] colors, int offset, int stride, int width, int height, Bitmap.Config config)


        //Создание чистого Bitmap
        //Эти методы позволяют создать чистый Bitmap без каких-либо данных.

        //display – объект DisplayMetrics, из которого Bitmap возьмет значение densityDpi
        //width, height – размеры создаваемого Bitmap
        //config – конфигурация
        //createBitmap(DisplayMetrics display, int width, int height, Bitmap.Config config)


        //Аналогичен методу 1, но без использования display
        //createBitmap(int width, int height, Bitmap.Config config)


        //Про density: Основная мысль тут такова: канва сравнивает density – свой и у Bitmap,
        // который она собирается рисовать. И если они различны, то выполняется
        // подгонка размеров.
        //Если убрать из манифеста опцию hardwareAccelerated, то основная канва
        // перестанет подгонять размер Bitmap

        //Mutable
        //Свойство mutable означает, что Bitmap может быть изменен (через обертку канвой,
        // методом setPixel, и т.п.). Соответственно immutable означает, что Bitmap
        // не может быть изменен, т.е. он readOnly.
    }


}