package com.example.googlemaplerning.evgeny.canvaslerning.region

import android.content.Context
import android.graphics.*
import android.view.View

class RegionView(val ccontext: Context) : View(ccontext) {


    var region: Region = Region()
//    var regionIterator : RegionIterator = RegionIterator()
    var paint : Paint = Paint()
    var path = Path()


    val op = Region.Op.UNION


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        //Region - это объект, который позволяет нам совмещать несколько фигур в одну,
        // используя различные режимы: объединение, пересечение

        paint.color = Color.BLACK

        //UNION, то итоговый регион будет являться объединением области
        // текущего региона и добавляемого прямоугольника.
//        region.set(Rect(200, 100, 600, 350))
//        region.op(Rect(300, 300, 700, 950), Region.Op.UNION)

        //удаляет пересечение
//        region.set(Rect(200, 100, 600, 350))
//        region.op(Rect(300, 300, 700, 950), Region.Op.XOR)

        //область первого прямоугольника за исключением пересечения его со вторым.
//        region.set(Rect(200, 100, 600, 350))
//        region.op(Rect(300, 300, 700, 950), Region.Op.DIFFERENCE)

        //тоже самое только наоборот
//        region.set(Rect(200, 100, 600, 350))
//        region.op(Rect(300, 300, 700, 950), Region.Op.REVERSE_DIFFERENCE)


        //только пересечение
//        region.set(Rect(200, 100, 600, 350))
//        region.op(Rect(300, 300, 700, 950), Region.Op.INTERSECT)

        //только второй прямоугольник
//        region.set(Rect(200, 100, 600, 350))
//        region.op(Rect(300, 300, 700, 950), Region.Op.REPLACE)

//        region.set(Rect(200, 100, 600, 350))
//        region.op(Rect(300, 300, 700, 950), Region.Op.REPLACE)
        path = region.boundaryPath




        paint.color = Color.GREEN
        canvas?.drawPath(path, paint)
    }


}