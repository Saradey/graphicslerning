package com.example.googlemaplerning.evgeny.canvaslerning.effect.path

import android.content.Context
import android.graphics.*
import android.view.View
import android.graphics.SumPathEffect
import android.graphics.PathEffect
import android.graphics.ComposePathEffect
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.graphics.DashPathEffect
import android.graphics.CornerPathEffect
import android.icu.lang.UCharacter.GraphemeClusterBreak.T






class PathEffectView(private val mainContext: Context) : View(mainContext) {


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        //У класса PathEffect есть несколько наследников, которые позволяют влиять
        // на рисуемые нами объекты.
        // на рисуемые нами объекты.

        //Эффект CornerPathEffect просто закругляет углы.
//        val p1 = Paint(Paint.ANTI_ALIAS_FLAG)
//        p1.style = Paint.Style.STROKE
//        p1.strokeWidth = 3f
//        p1.pathEffect = CornerPathEffect(25f)
//
//        val path = Path()
//        path.addRect(100f, 100f, 500f, 500f, Path.Direction.CW)


        //DiscretePathEffect позволяет получить ломанную линию из прямой. Полученная
        // ломанная линия будет состоять из фрагментов, а мы можем повлиять на длину
        // этих фрагментов (первый параметр конструктора) и степень излома
        // (второй параметр).
//        val p1 = Paint(Paint.ANTI_ALIAS_FLAG)
//        p1.style = Paint.Style.STROKE
//        p1.strokeWidth = 3f
//        p1.pathEffect = DiscretePathEffect(10f, 5f)
//        val path = Path()
//        path.addRect(100f, 100f, 500f, 500f, Path.Direction.CW)


        //С помощью DashPathEffect мы из сплошной линии можем получить прерывистую.
        // От нас требуется задать длину участка который будет прорисован и длину
        // участка, который прорисован не будет, т.е. «пусто». Далее эта комбинация
        // будет циклично использована для прорисовки всей линии.
//        val p1 = Paint(Paint.ANTI_ALIAS_FLAG)
//        p1.style = Paint.Style.STROKE
//        p1.strokeWidth = 3f
//        //только два значения длина участка которая будет прорисована
//        //длина участка которая будет пустая
//        p1.pathEffect = DashPathEffect(floatArrayOf(10f, 30f), 5f)
//        val path = Path()
//        path.addRect(100f, 100f, 500f, 500f, Path.Direction.CW)

        //PathDashPathEffect
        //PathDashPathEffect позволяет сделать пунктирную линию, но в качестве пунктира
        // можно использовать свой Path-объект.
//        val pathStamp = Path()
//        pathStamp.lineTo(-10f, -10f)
//        pathStamp.lineTo(10f, 0f)
//        pathStamp.lineTo(-10f, 10f)
//        //Метод close() закрывает контур.
//        pathStamp.close()
//
//        val p1 = Paint(Paint.ANTI_ALIAS_FLAG)
//        p1.style = Paint.Style.STROKE
//        p1.strokeWidth = 3f
//        //только два значения длина участка которая будет прорисована
//        //длина участка которая будет пустая
//
//        //path-объект, который будет использован в качестве пунктира
//        //расстояние между пунктирами
//        //отступ от начала
//        //стиль эффекта
//        p1.pathEffect = PathDashPathEffect(pathStamp, 20f, 0f, PathDashPathEffect.Style.ROTATE)
//        val path = Path()
//        path.addRect(100f, 100f, 500f, 500f, Path.Direction.CW)
//        val matrix = Matrix()
//        matrix.setScale(2f, 2f)
//        path.transform(matrix)

        // SumPathEffect и ComposePathEffect
        // Позволяют нам комбинировать два эффекта, которые подаются им на вход.
        // ComposePathEffect применит сначала один эффект, потом к получившемуся
        // результату – второй и выведет результат.
        // SumPathEffect – применит к искомой фигуре один эффект,
        // выведет результат, затем применит к искомой фигуре второй эффект
        // и выведет результат.
        val pe1 = CornerPathEffect(100f)
        val pe2 = DashPathEffect(floatArrayOf(20f, 5f), 0f)
        //имеет порядок очередности
        val pe3 = ComposePathEffect(pe2, pe1)
        val pe4 = SumPathEffect(pe1, pe2)
        val path = Path()
        path.addRect(100f, 100f, 500f, 500f, Path.Direction.CW)
        val p1 = Paint(Paint.ANTI_ALIAS_FLAG)
        p1.style = Paint.Style.STROKE
        p1.strokeWidth = 3f
        p1.pathEffect = pe3

        canvas?.drawPath(path, p1)
    }


}