package com.example.googlemaplerning.evgeny.canvaslerning.drawable

import android.graphics.drawable.ClipDrawable
import android.graphics.drawable.LevelListDrawable
import android.graphics.drawable.TransitionDrawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.googlemaplerning.evgeny.canvaslerning.R
import kotlinx.android.synthetic.main.main_actovoty_drawable_second.*

class MainActivityLevelListTransitionInsetClipScale : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_actovoty_drawable_second)

        //Тег <level-list> позволяет отображать Drawable в зависимости от значения
        // level. Рассмотрим пример, где будем отображать три разных Drawable:
        // если level=0, то  зеленый прямогуольник; если 1, то желтый; если 2,
        // то красный.
        //Менять уровень у Drawable мы можем методом setLevel. Ставим 0 (по умолчанию)
        //LevelListDrawable
//        imvDrawableSecond.setImageResource(R.drawable.main_level_list)
//        imvDrawableSecond.setImageLevel(2)

        //позволяет указать два Drawable и программно переключаться между ними с
        // fade-эффектом и указанием продолжительности перехода.


        //Тег <transition> позволяет указать два Drawable и программно переключаться
        // между ними с fade-эффектом и указанием продолжительности перехода.
        imvDrawableSecond.setImageResource(R.drawable.transition_my)
        val transitionDrawable: TransitionDrawable =
            imvDrawableSecond.drawable as TransitionDrawable
//
        transitionDrawable.startTransition(1000)
        transitionDrawable.reverseTransition(1000)

        //Насколько я понял – это просто обертка Drawable, которая позволяет
        // указать padding-отступы. Корневой тег – <inset>, атрибуты отступа
        // insetLeft, insetTop, insetRight, insetBottom.
//        класс InsetDrawable

        //Тег <clip> позволяет обрезать Drawable по горизонтальной и (или) вертикальной
        // оси. Какая часть картинки будет обрезана зависит от значения level.
        // Используемый диапазон тут от 0 до 10000, где 0 - картинка полностью
        // обрезана и не видна, 10000  - картинка видна полностью. Атрибут
        // gravity позволяет указать направление урезания.
//        val clipDrawable = ClipDrawable()

        //обрезаем
//        val clipDrawable: ClipDrawable = imvDrawableSecond.drawable!! as ClipDrawable
//        clipDrawable.level = 5000

        //<color> (класс ColorDrawable)
        //Цветовые ресурсы используются в виде значений в папке res/values,
        // например, в файле colors.xml. Но можно обращаться к цвету,
        // как drawable-ресурсу.


    }


}