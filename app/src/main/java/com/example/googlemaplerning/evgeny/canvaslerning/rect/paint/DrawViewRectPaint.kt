package com.example.googlemaplerning.evgeny.canvaslerning.rect.paint

import android.content.Context
import android.graphics.*
import android.view.View

class DrawViewRectPaint(private val con: Context) : View(con) {

    val rect = Rect()
    private val paint = Paint()

    val positionXClick: Int = 0
    val positionYClick: Int = 0

    var points: FloatArray? = floatArrayOf(100f, 50f, 150f, 100f, 150f, 200f, 50f, 200f, 50f, 100f)
    var rectf: RectF? = RectF(700f, 100f, 800f, 150f)


    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            it.drawColor(Color.BLACK)

            paint.color = Color.RED
            paint.strokeWidth = 10f

//            it.drawPoint(50f, 50f, paint)
//            it.drawLine(0f, 0f, 500f, 500f, paint)

//            рисуем круг с центром в(100, 200), радиус = 50
//            it.drawCircle(100f, 200f, 50f, paint)

            // левая верхняя точка (200,150), нижняя правая (400,200)
//            it.drawRect(200f, 150f, 400f, 200f, paint)
//
//            rect.set(200, 150, 400, 200)
//            it.drawRect(rect, paint)

            it.drawPoints(points!!, paint)


            paint.color = Color.GREEN
            //закругления
            canvas.drawRoundRect(rectf!!, 20f, 20f, paint)

            //смещаем rectf на 150 вниз
            //Т.е. просто опускаем прямоугольник вниз на 150.
            rectf?.offset(0f, 150f);

            //рисуем овал
            canvas.drawOval(rectf!!, paint)


            // смещаем rectf в (900,100) (левая верхняя точка)
            rectf?.offsetTo(900f, 100f)


            //Далее меняем размер прямоугольника rectf методом inset.
            // На вход метод принимает две дельты, на которые он уменьшит
            // прямоугольник по горизонтали (0) и вертикали (-25). Уменьшит
            // на -25 означает, увеличение на 25.
            rectf?.inset(0f, -25f);


            // рисуем дугу внутри прямоугольника rectf которая занимает предоставленный ей прямоугольник rectf.
            // с началом в 90, и длиной 270
            // соединение крайних точек через центр
            //Далее идут два угловых параметра: начало и длина, в нашем случае это 90 и 270 градусов.

            //Начало – угол, с которого дуга начинает рисоваться. Отсчет
            // ведется по часовой стрелке от точки «3 часа», если рассматривать
            // часовой циферблат. Т.е. если от трех часов отложить угол 90 градусов,
            // получится шесть часов. С этой точки и начнется рисоваться дуга.

            //Длина – это угол рисуемой дуги. Т.е. полный круг – это 360 градусов.
            // Соответственно 270 – три четверти круга. Если мы отложим три четверти
            // круга от 6 часов, то получим 3 часа. Такая дуга и должна получится:
            // от шести до трех часов по часовой стрелке.
            paint.color = Color.WHITE
            canvas.drawArc(rectf!!, 90f, 270f, true, paint)

            // смещаем коорднаты rectf на 150 вниз
            rectf?.offset(0F, 150f)

            // рисуем дугу внутри прямоугольника rectf
            // с началом в 90, и длиной 270
            // соединение крайних точек напрямую
            canvas.drawArc(rectf!!, 90f, 270f, false, paint)

            paint.textSize = 30f
            canvas.drawText("Hello World", 150f, 225f, paint)
            //выравнивание текста по левой стороне, будет слева от точки
            paint.textAlign = Paint.Align.RIGHT
            canvas.drawText("Hello World", 150f, 225f, paint)

            //размеры экрана
            paint.textSize = 80f
            canvas.drawText("width:${canvas.width} height:${canvas.height}", 1080f, 1584f, paint)


            //переключение
            // левая верхняя точка (200,150), нижняя правая (400,200)
            rect.set(200, 150, 400, 500)
            paint.style = Paint.Style.STROKE
            paint.color = Color.WHITE
            paint.strokeWidth = 10f
            canvas.drawRect(rect, paint)

//            Paint.ANTI_ALIAS_FLAG,
//            обеспечивающий сглаживание диагональных линий, рисуемых объектом Paint (снижая при этом производительность).
            // Чтобы сделать текст еще более гладким, можете использовать
            // флаг SUBPIXEL_TEXT_FLAG, который применяет субпиксельное сглаживание.
        }


    }


}
