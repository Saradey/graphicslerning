package com.example.googlemaplerning.evgeny.canvaslerning.colorFilter.colorMatrix

import android.content.Context
import android.graphics.*
import android.view.View





class ColorFilterColorMatrix(context: Context) : View(context) {


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        //Наследники ColorFilter позволяют нам оказывать влияние на цвет, используемый
        // при рисовании объектов.

        //Алгоритм немного нетривиальный, если вы незнакомы с матрицами из алгебры.
        //
        //Мы задаем матрицу такого вида
        //
        //rR, rG, rB, rA, rT
        //gR, gG, gB, gA, gT
        //bR, bG, bB, bA, bT
        //aR, aG, aB, aA, aT

        //4 строки, в каждой по 5 значений.
        //И пусть у нас есть некий цвет ARGB, к которому будем фильтр применять.
        //Фильтр возьмет текущие значение цвета и из них, используя матрицу, вычислит новые.

        //апример, новое значение красного (Rn) он посчитает так:
        //
        //Rn = R * rR + G * rG + B * rB + A * rA + rT

        //Т.е. значения исходного цвета (R,G,B,A) перемножаем на первые 4 значения
        // (rR, rG, rB, rA) из первой строки матрицы и прибавляем пятое значение (rT)
        // из этой же строки.

        val cmData = floatArrayOf(
            1f, 0f, 0f, 0f, 0f,
            0f, 1f, 0f, 0f, 0f,
            0f, 0f, 1f, 0f, 0f,
            0f, 0f, 0f, 1f, 0f
        )
        //От нас требуется только матрицу ему предоставить
        //Сейчас наша матрица выглядит следующим образом
        //
        //1, 0, 0, 0, 0,
        //0, 1, 0, 0, 0,
        //0, 0, 1, 0, 0,
        //0, 0, 0, 1, 0
        //
        //
        //
        //Если мы возьмем RGBA и применим матрицу, получим
        //
        //Rn = R * 1 + G * 0 + B * 0 + A * 0 + 0 = R
        //
        //Gn = R * 0 + G * 1 + B * 0 + A * 0 + 0 = G
        //
        //Bn = R *0 + G * 0 + B * 1 + A * 0 + 0 = B
        //
        //An = R * 0 + G * 0 + B * 0 + A * 1 + 0 = A

        val cmData2 = floatArrayOf(
            0f,
            0f,
            0f,
            0f,
            255f,
            0f,
            0f,
            0f,
            0f,
            0f,
            0f,
            0f,
            0f,
            0f,
            0f,
            0f,
            0f,
            0f,
            1f,
            0f
        )


        val cmData3 = floatArrayOf(
            1f,
            0f,
            0f,
            0f,
            0f,
            0f,
            0f,
            0f,
            0f,
            0f,
            0f,
            0f,
            0f,
            0f,
            0f,
            0f,
            0f,
            0f,
            1f,
            0f
        )

        // теперь давайте попробуем поменять прозрачность
        val cmData4 = floatArrayOf(
            1f,
            0f,
            0f,
            0f,
            0f,
            0f,
            1f,
            0f,
            0f,
            0f,
            0f,
            0f,
            1f,
            0f,
            0f,
            0f,
            0f,
            0f,
            0.3f,
            0f
        )

        //Черно-белая
        val cmData5 = floatArrayOf(
            0.3f,
            0.59f,
            0.11f,
            0f,
            0f,
            0.3f,
            0.59f,
            0.11f,
            0f,
            0f,
            0.3f,
            0.59f,
            0.11f,
            0f,
            0f,
            0f,
            0f,
            0f,
            1f,
            0f
        )

        //Инвертирование цветов
        val cmData6 = floatArrayOf(
            -1f,
            0f,
            0f,
            0f,
            255f,
            0f,
            -1f,
            0f,
            0f,
            255f,
            0f,
            0f,
            -1f,
            0f,
            255f,
            0f,
            0f,
            0f,
            1f,
            0f
        )

        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.style = Paint.Style.FILL_AND_STROKE
        paint.color = Color.RED
        val icon = BitmapFactory.decodeResource(context.resources, com.example.googlemaplerning.evgeny.canvaslerning.R.drawable.woman);

        //матрица и он знает какие преобразования цвета ему необходимо будет произвести.
//        val cm = ColorMatrix(cmData6)
        //setScale – позволяет нам указать на какие значения необходимо умножать RGBA значения цвета
        //Т.е. значения 1, 1, 0, 0.5f настроят матрицу так, что
        //
        //Rn = R * 1;
        //
        //Gn = G * 1;
        //
        //Bn = B * 0;
        //
        //An = A * 0.5f
        val cm = ColorMatrix()
        cm.setScale(1f, 1f, 0f, 0.5f)
        // setSaturation. Отвечает за насыщенность цветов.
        //Если задать 0, то получим черно-белую картинку.
        //Если например 0.5f, то будет половина насыщенности
        //По умолчанию значение этого метода = 1
        cm.setSaturation(0f)
        val filter = ColorMatrixColorFilter(cm)




        paint.colorFilter = filter

        canvas?.drawBitmap(icon, 50f, 50f, paint)


        val rect1 = Rect(50, 350, 350, 650)
        canvas?.drawRect(rect1, paint)

        paint.color = Color.GREEN
        val rect2 = Rect(50, 700, 350, 1000)
        canvas?.drawRect(rect2, paint)

        paint.color = Color.YELLOW
        val rect3 = Rect(50, 1050, 350, 1350)
        canvas?.drawRect(rect3, paint)


        //LightingColorFilter

        //PorterDuffColorFilter.



    }


}