package com.example.googlemaplerning.evgeny.canvaslerning.bitmap

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.os.Environment
import android.util.Log
import android.view.View
import com.example.googlemaplerning.evgeny.canvaslerning.R
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class ReadBitMap(context: Context) : View(context) {


    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas?) {
        //Когда мы читаем изображение с SD-карты в Bitmap, оно в памяти
        // занимает значительно больше, чем размер файла на SD. Потому
        // что на диске оно хранится в сжатом JPG или PNG формате.
        // А мы при чтении его распаковываем и получаем полновесный Bitmap.

        //Впечатляет! Наше изображение заняло в памяти 66 мегабайт!
        // Т.е. чтобы на экране отобразить изображение размером 300dp,
        // мы держим в памяти изображение размером 5712х2986 и весит
        // оно 66 мегабайт. Крайне неудачная реализация.
        // А уж если придется выводить на экран несколько таких картинок,
        // то OutOfMemory нам не избежать.

        //Тут нам помогут два параметра BitmapFactory.Options:
        //
        //inJustDecodeBounds – поможет нам узнать размер изображения, не занимая память
        //
        //inSampleSize – при чтении изображения уменьшит его размер в требуемое
        // кол-во раз, и на выходе мы получим уже уменьшенную копию, что значительно
        // сэкономит нам память. Этот коэффициент должен быть кратным двум.


        //// Читаем с inJustDecodeBounds=true для определения размеров
        //  final BitmapFactory.Options options = new BitmapFactory.Options();
        //  options.inJustDecodeBounds = true;
        //  BitmapFactory.decodeFile(path, options);

        //Также на вход идут reqWidth и reqHeight, в которые мы передаем желаемые
        // нам размеры изображения.
        //// Вычисляем inSampleSize
        //  options.inSampleSize = calculateInSampleSize(options, reqWidth,
        //      reqHeight);
        //
        //  // Читаем с использованием inSampleSize коэффициента
        //  options.inJustDecodeBounds = false;
        //  return BitmapFactory.decodeFile(path, options);

        //Поэтому мы можем использовать RGB_565 конфигурацию вместо дефолтовой ARGB_8888. Это уменьшит вес bitmap еще в два раза (Урок 157).
        //В метод decodeSampledBitmapFromResource добавим перед return следующую строку:
        //options.inPreferredConfig = Bitmap.Config.RGB_565;

        //Если вы пишете, например, приложение - графический редактор,
        // то ему реально нужно будет много памяти. В этом случае можно
        // в манифесте в тег <application> добавить параметр largeHeap="true".
        // Вашему приложению будет выделено памяти больше, чем обычно. Но это
        // должно быть реально последним средством перед которым вы оптимизировали
        // все, что можно.


    }


}