package com.example.googlemaplerning.evgeny.canvaslerning.drawable

import android.content.Context
import android.graphics.*
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.googlemaplerning.evgeny.canvaslerning.R

class ActivityBitmapShaderLinearGradientRadialGradientSweepGradient : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_bitmap_shader_linear_gradient)
        //Используя подклассы класса Shader мы получаем возможность "рисовать рисунком".
        setContentView(MainViewShader(this))


    }


    inner class MainViewShader(context: Context) : View(context) {

        lateinit var paint: Paint
        lateinit var bitmap: Bitmap


        override fun onDraw(canvas: Canvas?) {
//            bitmap = BitmapFactory.decodeResource(resources, R.drawable.woman)
//            //растянуть битма на всю канву
//            bitmap = Bitmap.createScaledBitmap(bitmap, 460, 680, true)
//            //Shader.TileMode.REPEAT Шейдер повторения
//            val shader = BitmapShader(bitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT)
//            //к шейдеру можно применить матрицу
//            val matrix = Matrix()
//            matrix.postScale(2f, 1.5f)
//            matrix.postRotate(45f)
//            shader.setLocalMatrix(matrix)
//            paint = Paint(Paint.ANTI_ALIAS_FLAG)
//            paint.shader = shader
//            canvas?.drawARGB(80, 102, 204, 255)
//            canvas?.drawRect(100f, 100f, 800f, 800f, paint)
//            canvas?.drawCircle(100f, 800f, 100f, paint)

            //LinearGradient
            //Этот шейдер позволяет нам получить градиент. У его класса есть два конструктора.
            // Оба просят от нас указать им TileMode и координаты линии, которая будет задавать
            // одновременно направление, начало и размер градиента.
//            val shader = LinearGradient(0f,0f, 100f, 20f, Color.RED, Color.GREEN, Shader.TileMode.MIRROR)
            //Другой конструктор позволяет задать массив цветов и их положений в градиенте.
//            val shader = LinearGradient(
//                0f,
//                0f,
//                100f,
//                20f,
//                intArrayOf(Color.RED, Color.BLUE, Color.GREEN),
//                null,
//                Shader.TileMode.MIRROR
//            )
            //задать позицию в ручную
//            val shader = LinearGradient(
//                120f,
//                0f,
//                380f,
//                0f,
//                intArrayOf(Color.RED, Color.BLUE, Color.GREEN),
//                floatArrayOf(0f, 0.5f, 1f),
//                Shader.TileMode.REPEAT
//            )
//            paint = Paint(Paint.ANTI_ALIAS_FLAG)
//            paint.shader = shader
//            canvas?.drawARGB(80, 102, 204, 255)
//            canvas?.drawRect(100f, 100f, 800f, 800f, paint)
//            canvas?.drawCircle(100f, 800f, 100f, paint)
            //Для класса RadialGradient указываем центр и радиус и градиент пойдет от центра к краям.
//            val shader =
//                RadialGradient(25f, 25f, 30f, Color.RED, Color.GREEN, Shader.TileMode.REPEAT)
//
//            paint = Paint(Paint.ANTI_ALIAS_FLAG)
//            paint.shader = shader
//            canvas?.drawARGB(80, 102, 204, 255)
//            canvas?.drawRect(100f, 100f, 800f, 800f, paint)
//            canvas?.drawCircle(100f, 800f, 100f, paint)
            // А SweepGradient – градиент, который идет по кругу вокруг центра.
//            val shader = SweepGradient(25f, 100f, Color.RED, Color.GREEN)
//            paint = Paint(Paint.ANTI_ALIAS_FLAG)
//            paint.shader = shader
//            canvas?.drawARGB(80, 102, 204, 255)
//            canvas?.drawRect(100f, 100f, 800f, 800f, paint)
//            canvas?.drawCircle(100f, 800f, 100f, paint)
        }
    }


}