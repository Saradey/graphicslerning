package com.example.googlemaplerning.evgeny.canvaslerning.porterDuffColorFilter

import android.content.Context
import android.graphics.Canvas
import android.graphics.PorterDuff
import android.view.View

class PorterDuffColorFilterView(context: Context) : View(context) {


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)


        //Механизм PorterDuffColorFilter похож на PorterDuffXfermode.
        // Только вместо двух картинок у нас будет картинка и цвет.
        // При создании объекта PorterDuffColorFilter вы указываете цвет,
        // и он будет играть роль SRC-картинки полностью залитой этим цветом.

        PorterDuff.Mode.MULTIPLY
        //MULTIPLY
        //Перемножение SRC и DST цветов. Умножение цвета на белый (1,1,1)
        // не меняет цвет, умножение на черный (0,0,0) – делает его черным.


        PorterDuff.Mode.DARKEN
        //DARKEN
        //
        //При расчете пикселов результата выбирается наиболее темный из
        // двух исходных: SRC и DST.

        PorterDuff.Mode.LIGHTEN
        //LIGHTEN
        //
        //При расчете пикселов результата выбирается наиболее светлый из двух
        // исходных: SRC и DST.

        PorterDuff.Mode.SCREEN
        //SCREEN
        //
        //Похож на MULTIPLY, только наоборот. При скрещении цвета с белым получается
        // белый, при скрещении с черным – цвет не меняется.

        PorterDuff.Mode.OVERLAY
        //OVERLAY
        //
        //Этот режим, к сожалению, не могу прокомментировать. Похоже на изменение
        // контрастности картинки.


    }


}