package com.example.googlemaplerning.evgeny.canvaslerning.measure.path

import android.content.Context
import android.graphics.*
import android.util.Log
import android.view.View


class MeasurePathView(private val mainContext : Context) : View(mainContext) {


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)


        // PathMeasure, весьма полезный в некоторых случаях инструмент, который умеет:
        //
        //- вычислять длину сегментов Path
        //
        //- определять, закрыт или открыт сегмент
        //
        //- получать координаты и угол наклона для указанной точки Path
        //
        //- выделять часть Path в отдельный объект

        val paint = Paint()
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = 5f
        paint.color = Color.parseColor("#00574B")

        val path = Path()
        path.moveTo(100f, 100f)
        path.rLineTo(150f, 100f)
        path.rLineTo(150f, -100f)
        //dx1 dy1 точка в самом низу
        //dx2 dy2 крайняя правая точка
        path.rQuadTo(150f, 200f, 300f, 0f)
        canvas?.drawPath(path, paint)

        val pMeasure: PathMeasure = PathMeasure(path, false)

        //берем размер path
//        length = pMeasure.getLength();

        // заполнит не матрицу, а два массива: pos – позиция, tan – наклон (cos и sin угла).
        //разместится в точке, которая находится на расстоянии distance и его угол
        // будет соответствовать углу наклона Path в этой точке.
        //Если из MainActivity.onCreate убрать флаги заголовка и полноэкранного режима,
        // то полученная матрица будет содержать некорректные значения смещения
        //не учитывающие высоту заголовка и верхней панели.
//        pMeasure.getPosTan()

        //Path может состоять из нескольких контуров и PathMeasure умеет их различать.
        do {
            Log.d("MeasurePathView", "${pMeasure.length} ${pMeasure.isClosed}")
        } while (pMeasure.nextContour())

    }




}