package com.example.googlemaplerning.evgeny.canvaslerning.matrix

import android.content.Context
import android.graphics.*
import android.view.View


class MatrixView(private val con: Context) : View(con) {

    val path = Path()
    val pathDST = Path()
    val paint = Paint()
    val matrixx = Matrix()
    val rectF = RectF()
    val rectfDst = RectF()
    val rectInt = RectF(200f, 800f, 500f, 1000f)

    init {

    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.drawColor(Color.WHITE)

        canvas?.let {

            paint.color = Color.BLACK
            paint.strokeWidth = 10f
            paint.style = Paint.Style.STROKE
//            path.addRect(300f, 150f, 450f, 200f, Path.Direction.CCW)


            // настраиваем матрицу на перемещение на 300 вправо и 200 вниз
//            it.translate(100f, 100f)
//            it.drawPath(path, paint)
//
//
//            it.translate(100f, 100f)
//            it.drawPath(path, paint)
//
//            //размер
//            it.translate(-200f, 300f)
//            it.scale(2f, 2f)
//            it.drawPath(path, paint)
//
//            //поворот
//            it.rotate(120f)
//            it.scale(0.5f, 0.5f)
//            it.translate(-400f, -200f)
//            it.drawPath(path, paint)


//            paint.color = Color.GREEN
//            //наклон
//            it.skew(0f, 0.5f)
//            it.drawPath(path, paint)


            //границы снеговика
//            path.addCircle(200f, 100f, 50f, Path.Direction.CW)
//            path.addCircle(200f, 225f, 75f, Path.Direction.CW)
//            path.addCircle(200f, 400f, 100f, Path.Direction.CW)
//            path.computeBounds(rectF, true) //рассчитать границы
//            it.drawPath(path, paint)
//            it.drawRect(rectF, paint)
//
//            rectfDst.set(450f, 250f, 800f, 600f)
//            //относительное преобразование
//            matrixx.setRectToRect(rectF, rectfDst, Matrix.ScaleToFit.START)
//            path.transform(matrixx, pathDST)
//            paint.color = Color.GREEN
//            it.drawPath(pathDST, paint)
//            it.drawRect(rectfDst, paint)
//
//            matrixx.reset()
//            matrixx.setPolyToPoly()

            path.addCircle(200f, 100f, 50f, Path.Direction.CW)
            path.addCircle(200f, 225f, 75f, Path.Direction.CW)
            path.addCircle(200f, 400f, 100f, Path.Direction.CW)
            path.computeBounds(rectF, true) //рассчитать границы
            it.drawPath(path, paint)
            it.drawRect(rectF, paint)

            //переместить на
//            matrixx.setTranslate(400f, 100f)
//            path.transform(matrixx)
//            it.drawPath(path, paint)
//            matrixx.reset()

//            //увеличить размер на
//            matrixx.setScale(2.6f, 2.5f)
//            path.transform(matrixx)
//            it.drawPath(path, paint)

            // настраиваем матрицу на поворот на 120 градусов
            // относительно точки (600,400)
//            matrixx.setRotate(120f, 300f, 300f)
//            path.transform(matrixx)
//            it.drawPath(path, paint)


            //Методы pre* добавляют трансформацию в самое начало списка
            // трансформаций матрицы. Методы post* - в самый конец.

            // перемещение до поворота
//            matrixx.setRotate(45f, 300f, 300f)
//            matrixx.preTranslate(200f, 700f)
//            path.transform(matrixx)
//            it.drawPath(path, paint)

            //setRectToRect
            //Этот метод берет два прямоугольника и определяет какие
            // преобразования необходимо выполнить над первым прямоугольником,
            // чтобы он полностью поместился во втором. Эти преобразования
            // записываются в матрицу и мы можем ее использовать.


//            matrixx.setRectToRect(rectF, rectInt, Matrix.ScaleToFit.START)
//            path.transform(matrixx)
//            //рамка
//            it.drawRect(rectInt, paint)
//            it.drawPath(path, paint)

            //Этот метод, кроме 4-х обычных операций (перемещение, изменение размера,
            // наклон, поворот), позволяет задать в матрице пятую операцию – перспективу.
            // Причем задаем мы их не явно, а указывая исходные и целевые точки.
            // По ним уже матрица сама вычисляет, какие преобразования необходимо
            // сделать.

            //- массив исходных координат
            //- позиция элемента в массиве исходных координат, с которого начинаем формировать точки
            //- массив целевых координат
            //- позиция элемента в массиве целевых координат, с которого начинаем формировать точки
            //- кол-во точек, которые метод setPolyToPoly возьмет из массивов и использует для настройки матрицы

            //Они являются координатами одной исходной точки (100,100).
            val src = floatArrayOf(100f, 100f)
            //Они являются координатами одной целевой точки
            val dst = floatArrayOf(500f, 600f)

            //Позиции элементов в массиве будем всегда указывать 0, т.е. начинаем формировать точки с первого элемента.
            //Кол-во точек указываем 1. Т.к. два элемента массива – это две координаты – это одна точка.
            //таким нетривиальным способом мы просто выполнили перемещение.
            matrixx.setPolyToPoly(src, 0, dst, 0, 1)
            path.transform(matrixx)
            it.drawPath(path, paint)

            //Теперь разберемся, какой смысл несут исходная и целевая точки.
            // Когда мы используем всего по одной исходной и целевой точке,
            // матрица настраивает трансформацию перемещения. Говоря простыми
            // словами, матрица настроится так, чтобы исходная точка (100,100)
            // после преобразования находилась в целевой точке (150,120).
            //азумеется, преобразование применилось не к одной точке, а ко всей фигуре


            //Одна точка (массив из двух координат) позволила нам задать перемещение.
            //
            //Две точки (массив из четырех координат) позволили нам задать перемещение,
            // поворот и изменение размера.
            //
            //Три точки (массив из шести координат) позволили нам задать перемещение,
            // поворот, изменение размера и наклон.
            //
            //Четыре точки (массив из восьми координат) позволили нам задать перемещение,
            // поворот, изменение размера, наклон и перспективу.

            //Метод setPolyToPoly кроме настройки матрицы возвращает нам boolean
            // значение. Этим он сообщает: получилось у него настроить матрицу или
            // требования были противоречивы и настройка невозможна.
        }
    }


}
