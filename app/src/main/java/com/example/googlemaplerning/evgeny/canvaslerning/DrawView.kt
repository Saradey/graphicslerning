package com.example.googlemaplerning.evgeny.canvaslerning

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.util.Log
import android.view.View

class DrawView(private val con: Context) : View(con) {


    //не вызывается постоянно, только если при создание или если активити на паузе
    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        Log.d("DrawView", "Вызов onDraw")
        canvas?.drawColor(Color.BLACK)

        //А в действительности это не очень хорошая практика,
        // т.к. это все будет идти в основном потоке.
        //для анимации будет постоянно вызывать onDraw
        //И правильнее будет реализовать такую
        // постоянную перерисовку через SurfaceView.
//        invalidate()
    }


}
