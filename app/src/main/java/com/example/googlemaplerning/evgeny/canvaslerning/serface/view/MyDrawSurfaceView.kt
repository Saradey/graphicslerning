package com.example.googlemaplerning.evgeny.canvaslerning.serface.view


import android.content.Context
import android.view.SurfaceHolder
import android.view.SurfaceView


class MyDrawSurfaceView(private val con: Context) : SurfaceView(con), SurfaceHolder.Callback {


    lateinit var threadDrawable: ThreadDrawable

    init {
        holder.addCallback(this)
    }


    //был изменен формат или размер SurfaceView
    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {

    }


    //вызывается перед тем, как SurfaceView будет уничтожен
    override fun surfaceDestroyed(holder: SurfaceHolder?) {
        threadDrawable.running = false
    }


    //SurfaceView создан и готов к отображению информации
    override fun surfaceCreated(holder: SurfaceHolder?) {
        threadDrawable = ThreadDrawable((getHolder()))
        threadDrawable.running = true
        threadDrawable.start()
    }


}
