package com.example.googlemaplerning.evgeny.canvaslerning.canva

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.view.View

class CanvaView(val ccontext: Context) : View(ccontext) {

    val paint = Paint()
    val rect = Rect(200, 300, 700, 500)


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        //Канва имеет свою матрицу, которая будет срабатывать для любого
        // объекта, который вы собираетесь нарисовать.
        //- translate (перемещение)
        //- scale (изменение размера)
        //- rotate (поворот)
        //- skew (наклон)

        //У канвы эти методы являются pre-методами.
        paint.color = Color.BLACK
        paint.strokeWidth = 10f
        canvas?.drawRect(rect, paint)
        //сохраняем
        //вы можете несколько раз вызывать save и все эти состояния будут
        // хранится в некоем стеке.
        canvas?.save()

        //по этому флагу мы можем вернуться к этому сохранению канвы
//        val flagSave = canvas?.save()

        paint.color = Color.GREEN
        canvas?.translate(100f, 100f)
        canvas?.drawRect(rect, paint)

        //сбрасываем до сохранненого
        canvas?.restore()
        //вернемся к сохранению по флагу минуя остальные
//        canvas?.restoreToCount(flagSave)

        paint.color = Color.YELLOW
        canvas?.translate(100f, 100f)
        canvas?.drawRect(rect, paint)


    }


}