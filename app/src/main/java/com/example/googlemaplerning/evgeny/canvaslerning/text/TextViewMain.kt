package com.example.googlemaplerning.evgeny.canvaslerning.text

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Typeface
import android.view.View

class TextViewMain(private val mainContext: Context) : View(mainContext) {


    val textStr = "Hello world"
    val textStyle = "text Style"
    val fontSize = 50f
    val fontPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    val with = 100f


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        fontPaint.textSize = fontSize
        fontPaint.style = Paint.Style.FILL


//        canvas?.drawText(textStr, 100f, 100f, fontPaint)

//        // ширина текста
//        width : Float = fontPaint.measureText(text);
//
//        // посимвольная ширина
//        val widths : Array  = new float[text.length()];
//        fontPaint.getTextWidths(text, widths);

//        - текст
//        - true, означает что пойдем по тексту вперед, начиная с первого символа. Если false, то пойдем с конца.
//        - ширину, которая будет ограничивать текст
//        - массив, для получения точного значения ширины
//        val cnt: Int = p.breakText(text, true, maxWidth, measuredWidth)

        // getTextBounds, который позволит получить вам прямоугольник, в который заключен текст.

        //стили текстов
//        // моноширинный
//        fontPaint.typeface = Typeface.create(Typeface.MONOSPACE, Typeface.NORMAL)
//        canvas?.drawText(textStyle, 100f, 100f, fontPaint)
//
//        canvas?.translate(100f, 100f)
//        fontPaint.typeface = Typeface.create(Typeface.SERIF, Typeface.NORMAL)
//        canvas?.drawText(textStyle, 0f, 100f, fontPaint)
//
//        canvas?.translate(0f, 100f)
//        fontPaint.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
//        canvas?.drawText(textStyle, 0f, 100f, fontPaint)

        //из типов:
        //
        //MONOSPACE – моноширинный, т.е. ширина всех символов одинакова
        //
        //SERIF – шрифт с засечками
        //
        //DEFAULT - шрифт по умолчанию
        //
        //
        //
        //и стилей:
        //
        //NORMAL – обычный
        //
        //BOLD – жирный
        //
        //BOLD_ITALIC – жирный курсивный
        //
        //ITALIC - курсивный

//        canvas?.translate(0f, 100f)
//        fontPaint.typeface = Typeface.createFromAsset()
//        canvas?.drawText(textStyle, 0f, 100f, fontPaint)

//        //расстянутый текст
//        fontPaint.typeface = Typeface.create(Typeface.DEFAULT, Typeface.NORMAL)
//        canvas?.translate(0f, 100f)
//        fontPaint.textScaleX = 2f
//        canvas?.drawText(textStyle, 0f, 100f, fontPaint)

        //наклонный
//        canvas?.translate(100f, 100f)
//        fontPaint.textSkewX = 0.1f
//        canvas?.drawText(textStyle, 0f, 100f, fontPaint)

        //подчеркивание
//        canvas?.translate(100f, 100f)
//        fontPaint.isUnderlineText = true
//        canvas?.drawText(textStyle, 0f, 100f, fontPaint)

        //вычеркивание
//        canvas?.translate(100f, 100f)
//        fontPaint.isStrikeThruText = true
//        canvas?.drawText(textStyle, 0f, 100f, fontPaint)
    }


}