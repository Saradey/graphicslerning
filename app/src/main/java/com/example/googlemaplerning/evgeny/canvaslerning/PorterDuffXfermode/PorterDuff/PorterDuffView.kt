package com.example.googlemaplerning.evgeny.canvaslerning.PorterDuffXfermode.PorterDuff

import android.content.Context
import android.graphics.*
import android.view.View


class PorterDuffView(context: Context) : View(context) {


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        // PorterDuff-режимы позволяют нам получать различные результаты при
        // наложении одного изображения на другое.

        //берутся значения цвета и прозрачности обоих изображений и по определенному
        // алгоритму рассчитываются итоговые значения.
        //Тут перечислены основные PorterDuff-режимы:
        //детальный пример https://www.w3.org/TR/2002/WD-SVG11-20020215/masking.html
        //https://developer.android.com/reference/android/graphics/PorterDuff.Mode.html

        //bitmapDst (DST), и поверх нее bitmapSrc (SRC)
        //отображается только SRC картинка, т.е. bitmapSrc
        val mode = PorterDuff.Mode.SRC

        //отображается только DST картинка, т.е. bitmapDst
        val mode2 = PorterDuff.Mode.DST

        //Ничего не отображается
        val mode3 = PorterDuff.Mode.CLEAR

        //SRC отображается над DST
        val mode4 = PorterDuff.Mode.SRC_OVER

        //DST отображается над SRC
        val mode5 = PorterDuff.Mode.DST_OVER

        //отображается часть SRC, которая пересекается с DST
        val mode6 = PorterDuff.Mode.SRC_IN

        //отображается часть DST, которая пересекается с SRC
        val mode7 = PorterDuff.Mode.DST_IN

        //отображается часть SRC, которая не пересекается с DST
        val mode8 = PorterDuff.Mode.SRC_OUT

        //отображается часть DST, которая не пересекается с SRC
        val mode9 = PorterDuff.Mode.DST_OUT

        //отображается DST, и поверх него часть SRC, которая пересекается с DST
        val mode10 = PorterDuff.Mode.SRC_ATOP

        //отображается SRC, и поверх него часть DST, которая пересекается с SRC
        val mode11 = PorterDuff.Mode.DST_ATOP

        //отображается SRC + DST, без части их пересечения
        val mode12 = PorterDuff.Mode.XOR

        //Сложение цветов
        val mode13 = PorterDuff.Mode.ADD


        //Но если поменять значения альфы, то результат будет другим.
        val porter = PorterDuffXfermode(mode)


    }


}