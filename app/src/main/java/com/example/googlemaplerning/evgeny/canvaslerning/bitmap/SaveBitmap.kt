package com.example.googlemaplerning.evgeny.canvaslerning.bitmap

import android.content.Context
import android.graphics.*
import android.util.Log
import android.view.View
import com.example.googlemaplerning.evgeny.canvaslerning.R
import java.io.File
import java.io.FileOutputStream


class SaveBitmap(context: Context) : View(context) {


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        //Если включить (true) этот параметр, то система не будет создавать Bitmap,
        // а только вернет информацию о изображение в следующих полях
        //inJustDecodeBounds
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        var bitmap = BitmapFactory.decodeResource(resources, R.drawable.woman, options)

        Log.d(
            "SaveBitmap",
            "outWidth = ${options.outWidth} outHeight = ${options.outHeight} outMimeType = ${options.outMimeType}"
        )
        //не нарисует
        //bitmap = null, width = 48, height = 48, mimetype = image/png
//        canvas?.drawBitmap(bitmap, 50f, 50f, Paint())

        //Позволяет указать коэффициент уменьшения размера изображения при чтении.
        // Он должен быть кратным 2. Если зададите другое число, то оно будет
        // изменено на ближайшее число меньшее вашего и кратное 2.
        //inSampleSize
        val options2 = BitmapFactory.Options()
        options2.inSampleSize = 2
        val bitmap2 = BitmapFactory.decodeResource(resources, R.drawable.woman, options2)
        Log.d(
            "SaveBitmap",
            "inSampleSize width:${bitmap2?.width} height:${bitmap2?.height}"
        )
        canvas?.drawBitmap(bitmap2, 50f, 50f, Paint())

        //Параметры inJustDecodeBounds и inSampleSize можно использовать для чтения
        //больших изображений. Т.е. если вы сразу решите считать большое изображение
        //в Bitmap, вы можете занять, тем самым, слишком много памяти или вообще
        //получить OutOfMemory. Поэтому следует сначала получить данные о размерах
        //картинки, а затем с коэффициентом сжатия считать ее в Bitmap примерно
        //нужного размера.


        //Если передать в этот параметр Bitmap-объект, то он и будет использован
        //для получения результата вместо создания нового Bitmap-объекта.
        //inBitmap
        val tempBitmap = Bitmap.createBitmap(50, 50, Bitmap.Config.ARGB_8888)
        val options3 = BitmapFactory.Options()
        options.inBitmap = tempBitmap
        val bitmap3 = BitmapFactory.decodeResource(resources, R.drawable.woman, options3)
        canvas?.translate(200f, 200f)
        canvas?.drawBitmap(bitmap3, 0f, 0f, Paint())

        //Указание желаемой конфигурации Bitmap.Config.
        //inPreferredConfig

        //Задает density-значение для Bitmap, аналогично методу setDensity.
        // Для задания значения используйте константы DENSITY* класса DisplayMetrics.
        //inDensitys

        //Позволяет системе временно удалить содержимое созданного
        //Bitmap из памяти в случае нехватки таковой. Когда изображение
        //снова понадобится (например при выводе на экран), оно будет
        //восстановлено из источника. Т.е. жертвуем производительностью
        //в пользу памяти.
        //inPurgeable

        //Если true, то Bitmap хранит ссылку на источник, иначе – данные источника.
        //Но даже если true, то вполне может быть, что по усмотрению системы будут
        //храниться данные, а не ссылка. Этот параметр актуален только при включенном
        //inPurgeable.
        //inInputShareable

        //Попытка сгладить цвета, если текущей цветовой палитры не достаточно
        // для отображения оригинальных цветов изображения
        //inDither


        //Если true, то мы получим mutable Bitmap
        //inMutable


        //Включение более качественного декодирования в ущерб скорости
        //inPreferQualityOverSpeed


        //Доступен с API Level 19. Дает возможность выключить premultiplied-режим.
        // Если режим включен (по умолчанию), то RGB компоненты в пикселах сразу
        // рассчитаны с учетом альфа-компонента (для лучшей производительности).
        // Канва принимает Bitmap только в таком режиме. В хелпе сказано, что
        // выключение режима может понадобиться для специфических задач:
        // RenderScript  и OpenGL.
        //inPremultiplied


        //Здесь можем указать свой временный массив, который будет использован в процессе декодирования
        //inTempStorage


        //По этой метке можно определить был ли процесс декодирования отменен методом
        // requestCancelDecode
        //mCancel


        //Как сохранить Bitmap в файл
        //Метод compress позволяет сохранить Bitmap в разных форматах в исходящий поток. На вход принимает:
        //- формат (JPG, PNG, WEBP)
        //- качество сжатия, от 0 (наихудшее) до 100 (наилучшее)
        //- поток

        val bitmapIcon = BitmapFactory.decodeResource(resources, R.drawable.woman)

        val bitmap5 = Bitmap.createBitmap(500, 500, Bitmap.Config.RGB_565)
        val canvasBitmap5 = Canvas(bitmap5)
        canvasBitmap5.drawColor(Color.RED)
        canvasBitmap5.drawBitmap(bitmapIcon, 0f, 0f, Paint())
        canvasBitmap5.drawText("Кириллица", 100f, 50f, Paint().apply {
            color = Color.YELLOW
            textSize = 20f
        })
        canvas?.translate(0f, 100f)
        canvas?.drawBitmap(bitmap5, 0f, 0f, Paint())


        //сохраняем
        val file = File(
            "savedBitmap.png"
        )


        try {
            var fos: FileOutputStream? = null

            fos = FileOutputStream(file)

            Thread({
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
            }).start()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


}